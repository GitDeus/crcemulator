﻿using Newtonsoft.Json;

namespace NumonixCRCEmulator
{
    public class InstallationKeyEcryptedSessionData
    {
        [JsonProperty(PropertyName = "key")]
        public string Key { get; set; }
        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }
    }
}