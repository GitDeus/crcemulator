﻿using Newtonsoft.Json;

namespace NumonixCRCEmulator
{
    public class InstallationKeyEncryptedSessionData
    {
        [JsonProperty(PropertyName = "key")]
        public string Key { get; set; }

        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }
    }
}