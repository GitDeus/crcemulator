﻿using System;
using Newtonsoft.Json;

namespace NumonixCRCEmulator
{
    public class InstallationKeyOut
    {
        public InstallationKeyOut(Guid id, string endpoint, Guid tenantId)
        {
            Id = id;
            Endpoint = endpoint;
            TenantId = tenantId;
        }
        public InstallationKeyOut(Guid id, string endpoint, Guid tenantId, string publicKey)
        {
            Id = id;
            Endpoint = endpoint;
            TenantId = tenantId;
            PublicKey = publicKey;
        }

        public InstallationKeyOut()
        {

        }

        [JsonProperty(PropertyName = "id")]
        public Guid Id { get; set; }
        [JsonProperty(PropertyName = "endpoint")]
        public string Endpoint { get; set; }
        [JsonProperty(PropertyName = "tenantId")]
        public Guid TenantId { get; set; }
        [JsonProperty(PropertyName = "publicKey")]
        public string PublicKey { get; set; }
    }
}