﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace NumonixCRCEmulator
{
    public class InstallationKeyValidationInfo
    {
        public InstallationKeyValidationInfo()
        {
            StatusCodes = new List<InstallationKeyValidationStatus>();
        }

        [JsonProperty(PropertyName = "statusCodes")]
        public List<InstallationKeyValidationStatus> StatusCodes { get; private set; }

        [JsonProperty(PropertyName = "success")]
        public bool Success => StatusCodes.Any(a => a == InstallationKeyValidationStatus.Success);

        [JsonProperty(PropertyName = "data")]
        public InstallationKeyEcryptedSessionData Data { get; set; }

    }
}