﻿using NumonixCRCEmulator.Encryption;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using JsonConvert = Newtonsoft.Json.JsonConvert;
using RSA = NumonixCRCEmulator.Encryption.RSA;

namespace NumonixCRCEmulator
{
    public partial class Program
    {
        private const int MIN_INSTALLATION_KEY_LENGTH = 30;

        private static void Main(string[] args)
        {
            Console.Write("Call Recording Component Emulator\r\n");

            var help = "\tv - validation of the installation key\r\n\te - key exchange\r\n\tu - upload mock wav call recording file to server\r\n";

            Console.Write(help);

            var quitNow = false;
            while (!quitNow)
            {
                var command = Console.ReadLine();
                switch (command)
                {
                    case "u":

                        if (!File.Exists(Environment.CurrentDirectory + "/token")) Console.WriteLine("\t\r\n Error: Authorization token not found!");
                        if (!File.Exists(Environment.CurrentDirectory + "/installationKey"))
                        {
                            Console.WriteLine("\t\r\n Error: Installation key not found!");
                        }
                        else
                        {
                            var installationKeyBase64 = File.ReadAllText(Environment.CurrentDirectory + "/installationKey");
                            if (!InstallationKeyIsValid(installationKeyBase64))
                            {
                                Console.Write("\t\r\n Error: Invalid installation key!");
                            }

                            var installationKey = JsonConvert.DeserializeObject<InstallationKeyOut>(Encoding.UTF8.GetString(Convert.FromBase64String(installationKeyBase64)));

                            var baseEndpoint = installationKey.Endpoint;

                            var clientPrivateKey = File.ReadAllText(Environment.CurrentDirectory + "/clientPrivateKey");

                            var sessionPublicKey = File.ReadAllText(Environment.CurrentDirectory + "/sessionPK");

                            var authToken = File.ReadAllText(Environment.CurrentDirectory + "/token");

                            try
                            {
                                var privateClientKey = RSAKeys.ImportPrivateKey(clientPrivateKey);

                                var serverRsa = RSAKeys.ImportPublicKey(sessionPublicKey);


                                var mockFileInfo = new CallRecordingFileInfo
                                {
                                    Name = Guid.NewGuid().ToString(),
                                    CallStartTime = DateTime.UtcNow,
                                    CallerPhoneNumber = "+1 212-732-5692",
                                    Category = "Technical Issue",
                                    Extension = ".wav",
                                    IncomingPhoneNumber = "+1 212-279-1289",
                                    SupportLevel = "L2 Support"
                                };


                                var secret = Security.GenerateRandomString();
                                var encryptedMetadata =
                                    AES.Encrypt(
                                        JsonConvert.SerializeObject(mockFileInfo), secret);

                                var encryptedAesSecret = RSA.Encrypt(secret, serverRsa);

                                var metadataResponse = AddMetadata(baseEndpoint, encryptedMetadata, encryptedAesSecret, authToken);

                                if (metadataResponse.StatusCode == HttpStatusCode.Unauthorized)
                                {
                                    Console.WriteLine("\t\r\n Error: Session has expired");
                                }
                                else if (metadataResponse.IsSuccessStatusCode)
                                {
                                    var encryptedResponse = metadataResponse.Content.ReadAsAsync<EncryptedString>()
                                        .GetAwaiter()
                                        .GetResult();

                                    var serverSecret = RSA.Decrypt(encryptedResponse.Key, privateClientKey);

                                    var metadataId = JsonConvert.DeserializeObject<Guid>(AES.Decrypt(encryptedResponse.Encrypted, serverSecret));

                                    Console.WriteLine("\t\r\n Metadata success added!\tmetadataId:" + metadataId);
                                    Console.WriteLine("\t\r\n Uploading wav file...");
                                    #region Upload call recording wav file 

                                    var fileName = "audio.wav"; ;

                                    var fileBytes = File.ReadAllBytes(Environment.CurrentDirectory + "/" + fileName);

                                    var callRecordingEncryptedBytes = AES.Encrypt(fileBytes, secret);

                                    var uploadResponse = UploadCallRecording(baseEndpoint, encryptedAesSecret, metadataId, authToken, fileName, callRecordingEncryptedBytes);

                                    if (uploadResponse.StatusCode == HttpStatusCode.Unauthorized)
                                        Console.WriteLine("\t\r\n Current session has expired");
                                    else if (uploadResponse.IsSuccessStatusCode)
                                        Console.WriteLine("\t\r\n Success upload");
                                    else
                                        Console.WriteLine("\t\r\n Error: " + uploadResponse.StatusCode + "\t\r\n" + uploadResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult());

                                    #endregion

                                }
                                else
                                {
                                    Console.WriteLine("\t\r\n Error: " + metadataResponse.StatusCode + "\t\r\n" +
                                                      metadataResponse.ReasonPhrase);
                                    return;
                                }

                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("\t\r\n Error: session key not found");
                                Console.WriteLine("\t\r\n" + e.Message);
                            }
                        }
                        break;


                    case "e":

                        if (File.Exists(Environment.CurrentDirectory + "/token"))
                        {
                            var token = File.ReadAllText(Environment.CurrentDirectory + "/token");
                            Console.WriteLine("\t\r\n Session token is already there \r\n{0}", token);
                            Console.WriteLine("\t\r\n Press any key ...");
                            Console.ReadKey();
                        }

                        Console.Write("\t\r\n Please paste the installation key: ");

                        var inputexchange = ReadKey();

                        if (!InstallationKeyIsValid(installationKeyBase64: inputexchange))
                        {
                            Console.Write("\t\r\n Error: Invalid installation key!");
                        }
                        else
                        {
                            try
                            {
                                var json = Encoding.UTF8.GetString(Convert.FromBase64String(inputexchange));
                                var installationKey = JsonConvert.DeserializeObject<InstallationKeyOut>(json);


                                using (var clientRsa = new RSACryptoServiceProvider(2048))
                                {

                                    var privateClientKey = RSAKeys.ExportPrivateKey(clientRsa);
                                    var publicClientKey = RSAKeys.ExportPublicKey(clientRsa);


                                    var serverRsa = RSAKeys.ImportPublicKey(installationKey.PublicKey);
                                    var secret = Security.GenerateRandomString();
                                    var encryptedClientKeyInfo = new ClientKeyInfo(installationKey.Id,
                                        RSA.Encrypt(secret, serverRsa),
                                        AES.Encrypt(publicClientKey, secret));


                                    var exchangeResult = Exchange(installationKey.TenantId, installationKey.Endpoint, privateClientKey, encryptedClientKeyInfo)
                                        .GetAwaiter()
                                        .GetResult();

                                    Console.WriteLine("\t\r\n Installation Key Id :{0}\t\r\n {1} \t\r\n", installationKey.Id, !exchangeResult.Success ? string.Join("\t\t\r ", exchangeResult.StatusCodes.Select(d =>
                                        $"Status code {(int)d} : {d.ToString()}")) : "Success");

                                    Console.WriteLine("\t\r\n token {0}", exchangeResult.Data.Token);

                                    File.WriteAllText(Environment.CurrentDirectory + "/sessionPK", exchangeResult.Data.Key);
                                    File.WriteAllText(Environment.CurrentDirectory + "/token", exchangeResult.Data.Token);
                                    File.WriteAllText(Environment.CurrentDirectory + "/installationKey", inputexchange);
                                    File.WriteAllText(Environment.CurrentDirectory + "/clientPrivateKey", privateClientKey);
                                }

                            }
                            catch (Exception e)
                            {
                                Console.Write("\t\r\n Invalid installation key!");
                                Console.WriteLine("\t\r\n {0}", e.Message);
                            }
                            Console.Write(help);

                        }

                        break;
                    case "v":
                        Console.Write("\t\r\n Please paste the installation key: ");

                        var inputValidation = ReadKey();

                        if (string.IsNullOrEmpty(inputValidation) || inputValidation.Length < 30)
                        {
                            Console.Write("\t\r\n Invalid installation key!");
                        }
                        else
                        {
                            try
                            {
                                var json = Encoding.UTF8.GetString(Convert.FromBase64String(inputValidation));
                                var installationKey = JsonConvert.DeserializeObject<InstallationKey>(json);
                                var validationResult = Validation(installationKey.TenantId, installationKey.Endpoint, inputValidation)
                                    .GetAwaiter()
                                    .GetResult();
                                Console.WriteLine("\t\r\n Installation Key Id :{0}\t\r\n {1} \t\r\n", installationKey.Id, !validationResult.Success ? string.Join("\t\t\r ", validationResult.StatusCodes.Select(d =>
                                    $"Status code {(int)d} : {d.ToString()}")) : "Success");
                            }
                            catch (Exception e)
                            {
                                Console.Write("\t\r\n Invalid InstallationKey!");
                                Console.WriteLine("\t\r\n {0}", e.Message);
                            }

                            Console.Write(help);

                        }

                        break;

                    case "h":
                        Console.Write(help);
                        break;

                    case "q":
                        quitNow = true;
                        break;

                    default:
                        Console.WriteLine("Unknown Command " + command);
                        break;
                }
            }

            Console.ReadLine();
        }

        public static bool InstallationKeyIsValid(string installationKeyBase64)
        {
            return !string.IsNullOrEmpty(installationKeyBase64) && installationKeyBase64.Length >= MIN_INSTALLATION_KEY_LENGTH;
        }

        #region Call recording
        public static HttpResponseMessage AddMetadata(string baseEndpoint, string encryptedData, string encryptedAesSecret, string authToken)
        {
            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMinutes(1) })
            {
                httpClient.DefaultRequestHeaders.Add("X-Session", authToken);
                return httpClient.PostAsync(baseEndpoint + "callrecording/metadata",
                        new StringContent(
                            JsonConvert.SerializeObject(
                                new
                                {
                                    message = new
                                    {
                                        key = encryptedAesSecret,
                                        encrypted = encryptedData
                                    }
                                }), Encoding.UTF8, "application/json"))
                    .GetAwaiter()
                    .GetResult();
            }
        }

        public static HttpResponseMessage UploadCallRecording(string baseEndpoint, string encryptedAesSecret, Guid metadataId, string authToken, string fileName, byte[] callRecordingEncryptedBytes)
        {
            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMinutes(60) }) //For large files
            {
                httpClient.DefaultRequestHeaders.Add("X-Session", authToken);

                var content = new MultipartFormDataContent
                {
                    { new StringContent(metadataId.ToString()), "metadataId"},
                    { new StringContent(encryptedAesSecret), "secret"},
                    { new ByteArrayContent(callRecordingEncryptedBytes), "audio", fileName}
                };

                var uploadResponse = httpClient.PostAsync(baseEndpoint + "callrecording", content)
                    .GetAwaiter()
                    .GetResult();

                return uploadResponse;
            }
        }
        #endregion


        public static async Task<InstallationKeyValidationInfo> Exchange(Guid tenantId, string mainUrl, string privateRsaKey, ClientKeyInfo clientKeyInfo)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("tenantId", tenantId.ToString());
                var response = await httpClient.PostAsync(mainUrl + "installationkey/exchange", new StringContent(JsonConvert.SerializeObject(clientKeyInfo), Encoding.UTF8, "application/json"));
                var encrypted = await response.Content.ReadAsAsync<EncryptedString>().ConfigureAwait(false);
                var secret = RSA.Decrypt(encrypted.Key, RSAKeys.ImportPrivateKey(privateRsaKey));
                return JsonConvert.DeserializeObject<InstallationKeyValidationInfo>(AES.Decrypt(encrypted.Encrypted, secret));

            }
        }

        public static async Task<InstallationKeyValidationInfo> Validation(Guid tenantId, string mainUrl, string installationKey)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("tenantId", tenantId.ToString());

                var response = await httpClient.PostAsync(mainUrl + "installationkey/validation", new StringContent(JsonConvert.SerializeObject(installationKey), Encoding.UTF8, "application/json")).ConfigureAwait(false);
                return await response.Content.ReadAsAsync<InstallationKeyValidationInfo>().ConfigureAwait(false);

            }
        }

        private static string ReadKey()
        {
            var inputStream = Console.OpenStandardInput(4096);
            var bytes = new byte[4096];
            var outputLength = inputStream.Read(bytes, 0, 4096);
            var chars = Encoding.UTF7.GetChars(bytes, 0, outputLength);
            return new string(chars);
        }
    }
}
