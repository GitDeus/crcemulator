﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace NumonixCRCEmulator
{
    public class Security
    {
        public const string ALL_RANDOM_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        public const int DEFAULT_SECRET_LENGTH = 32;

        private static Random simpleRNG;

        /// <summary>
        /// generate a cryptographically sufficiently random string of specified length
        /// </summary>
        /// http://stackoverflow.com/a/32932758/2860309
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GenerateRandomString(int length = DEFAULT_SECRET_LENGTH, string validCharacters = null)
        {
            if (validCharacters == null)
            {
                validCharacters = ALL_RANDOM_CHARACTERS;
            }

            StringBuilder res = new StringBuilder();
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                byte[] uintBuffer = new byte[sizeof(uint)];

                while (length-- > 0)
                {
                    rng.GetBytes(uintBuffer);
                    uint num = BitConverter.ToUInt32(uintBuffer, 0);
                    res.Append(validCharacters[(int)(num % (uint)validCharacters.Length)]);
                }
            }

            return res.ToString();
        }

        /// <summary>
        /// create a readable and reasonably secure code of practical length
        /// </summary>
        /// <returns></returns>
        public static string GenerateVerificationCode()
        {

            if (simpleRNG == null)
            {
                simpleRNG = new Random();
            }

            // code length varies between 4 and 8
            int codeLength = simpleRNG.Next(4, 9);

            // secret must not include ambiguous characters like O/0, 1/l
            return GenerateRandomString(codeLength, "23456789abcdefghijkmnpqrstuvwxyz");
        }

        public static string GenerateHash(string plainText)
        {
            byte[] bPlainText = Encoding.UTF8.GetBytes(plainText);

            HashAlgorithm algorithm = new SHA256Managed();

            return Convert.ToBase64String(algorithm.ComputeHash(bPlainText));
        }
    }
}
