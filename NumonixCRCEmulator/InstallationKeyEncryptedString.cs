﻿using System;
using Newtonsoft.Json;

namespace NumonixCRCEmulator
{
    public class ClientKeyInfo
    {
        public ClientKeyInfo(Guid installationKeyId, string encryptedAesKey, string encryptedRsaPublicKey)
        {
            this.InstallationKeyId = Guid.Empty == installationKeyId ? throw new ArgumentException($"{nameof(installationKeyId)} is missing") : installationKeyId;
            this.EncryptedRsaPublicKey = string.IsNullOrEmpty(encryptedRsaPublicKey) ? throw new ArgumentException($"{nameof(EncryptedRsaPublicKey)} is missing") : encryptedRsaPublicKey;
            this.EncryptedAesKey = string.IsNullOrEmpty(encryptedAesKey) ? throw new ArgumentException($"{nameof(encryptedAesKey)} is missing") : encryptedAesKey;
        }

        [JsonProperty(PropertyName = "encryptedAesKey")]
        public string EncryptedAesKey { get; set; }

        [JsonProperty(PropertyName = "encryptedRsaPublicKey")]
        public string EncryptedRsaPublicKey { get; set; }

        [JsonProperty(PropertyName = "installationKeyId")]
        public Guid InstallationKeyId { get; set; }

    }
}