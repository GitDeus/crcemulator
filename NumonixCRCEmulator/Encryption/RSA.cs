﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace NumonixCRCEmulator.Encryption
{
    public class RSA
    {
        public const bool OAEP_PADDING = true;
        /// <summary>
        /// PKCS1 padding is required for most encryption using JavaScript packages
        /// </summary>
        public const bool PKCS1_PADDING = false;

        /// <summary>
        /// encrypt the plaintext string with the given public key
        /// </summary>
        /// <param name="plaintext"></param>
        /// <param name="csp"></param>
        /// <returns></returns>
        public static string Encrypt(
            string plaintext,
            RSACryptoServiceProvider csp
        )
        {
            return Convert.ToBase64String(
                csp.Encrypt(
                    Encoding.UTF8.GetBytes(plaintext),
                    PKCS1_PADDING
                )
            );
        }

        /// <summary>
        /// decrypt the encrypted string with the given private key
        /// </summary>
        /// <param name="encrypted"></param>
        /// <param name="csp"></param>
        /// <returns></returns>
        public static string Decrypt(
            string encrypted,
            RSACryptoServiceProvider csp
        )
        {
            return Encoding.UTF8.GetString(
                csp.Decrypt(
                    Convert.FromBase64String(encrypted),
                    PKCS1_PADDING
                )
            );
        }

    }
}