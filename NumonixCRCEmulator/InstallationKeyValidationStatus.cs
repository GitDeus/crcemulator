﻿namespace NumonixCRCEmulator
{
    public enum InstallationKeyValidationStatus
    {
        Success = 0,
        LicenseLimitReached = 1,// "License limit reached";
        Expired = 2,// "Installation key expired";
        Invalid = 3,//"Installation key not found";
        Deactivated = 4//"Installation key deactivated";
    }
}