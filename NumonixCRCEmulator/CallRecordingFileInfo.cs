﻿using System;

namespace NumonixCRCEmulator
{

    public class CallRecordingFileInfo
    {
        public string Name { get; set; }

        public string IncomingPhoneNumber { get; set; }

        public string CallerPhoneNumber { get; set; }

        public string Extension { get; set; }

        public DateTime CallStartTime { get; set; }

        public string Category { get; set; }

        public string SupportLevel { get; set; }
    }
}
