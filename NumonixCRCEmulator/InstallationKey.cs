﻿using System;

namespace NumonixCRCEmulator
{
    public class InstallationKey
    {
        public Guid Id { get; set; }
        public string Endpoint { get; set; }
        public Guid TenantId { get; set; }
        public int MaxNumberLicenses { get; set; }
        public int NumberOfUsedLicenses { get; set; }
        public DateTime Expiration { get; set; }
        public InstallationKeyStatus Status { get; set; }

        public string PublicKey { get; set; }

    }
}