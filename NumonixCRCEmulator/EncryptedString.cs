﻿using System;
using Newtonsoft.Json;

namespace NumonixCRCEmulator
{
    /// <summary>
    /// DTO Object
    /// </summary>
    public class EncryptedString
    {
        public EncryptedString(string key, string encrypted)
        {
            this.Encrypted = string.IsNullOrEmpty(encrypted) ? throw new ArgumentException($"{nameof(encrypted)} is missing") : encrypted;
            this.Key = string.IsNullOrEmpty(key) ? throw new ArgumentException($"{nameof(key)} is missing") : key;
        }
        [JsonProperty(PropertyName = "key")]
        public string Key { get; }
        [JsonProperty(PropertyName = "encrypted")]
        public string Encrypted { get; }
        [JsonProperty(PropertyName = "timeout")]
        public long Timeout { get; set; }
    }
}