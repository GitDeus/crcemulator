﻿using System.Collections.Generic;
using NumonixCRCEmulator;

namespace NumonixCRCEmulatorApp
{
    public class DemoWizard
    {
        public Step[] Steps = {};
    }

    public class Step
    {
        public State Status { get; set; }
        public string Title { get; set; }
        public Dictionary<InstallationKeyValidationStatus, string> Errors { get; set; }
    }

}