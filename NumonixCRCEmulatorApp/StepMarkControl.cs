﻿using System;
using System.Windows.Forms;

namespace NumonixCRCEmulatorApp
{
    public partial class StepMarkControl : UserControl
    {
        public StepMarkControl()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        public void StatusChanges(object sender, Step step)
        {
            Status_Description.Text = step.Title;
            switch (step.Status)
            {
                case State.Success:
                    Success_Status_Image.Visible = true;
                    Error_Status_Img.Visible = false;
                    break;
                    
                case State.Error:
                    Success_Status_Image.Visible = false;
                    Error_Status_Img.Visible = true;
                    break;

                default:
                    Error_Status_Img.Visible = false;
                    Success_Status_Image.Visible = false;
                    break;
            
            }
        }

        private void Status_Description_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
