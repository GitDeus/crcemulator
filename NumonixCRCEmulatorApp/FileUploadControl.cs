﻿using System;
using System.IO;
using System.Windows.Forms;

namespace NumonixCRCEmulatorApp
{
    public partial class FileUploadControl : UserControl
    {
        public event EventHandler<FileUploadEventArgs> FileLoad;

        public string filter = "WAV|*.wav";

        private OpenFileDialog OpenFileDialog = new OpenFileDialog();

        public FileUploadControl()
        {
            InitializeComponent();
        }

        private void Select_File_Btn_Click(object sender, EventArgs e)
        {
            OpenFileDialog.Filter = filter;
            if (OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                File_Text.Text = OpenFileDialog.SafeFileName;
                FileLoad?.Invoke(this, new FileUploadEventArgs(OpenFileDialog.SafeFileName, File.ReadAllBytes(OpenFileDialog.FileName)));
            }
        }
    }
}
