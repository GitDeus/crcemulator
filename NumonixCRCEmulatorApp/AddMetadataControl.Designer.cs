﻿namespace NumonixCRCEmulatorApp
{
    partial class AddMetadataControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.File_Name_Label = new System.Windows.Forms.Label();
            this.File_Name = new System.Windows.Forms.TextBox();
            this.Call_Start_Time_Label = new System.Windows.Forms.Label();
            this.CallerPhoneNumber = new System.Windows.Forms.TextBox();
            this.Caller_Phone_Number_Label = new System.Windows.Forms.Label();
            this.Category = new System.Windows.Forms.TextBox();
            this.Category_Label = new System.Windows.Forms.Label();
            this.Extension = new System.Windows.Forms.TextBox();
            this.Extension_Label = new System.Windows.Forms.Label();
            this.IncomingPhoneNumber = new System.Windows.Forms.TextBox();
            this.Incoming_Phone_Number_Label = new System.Windows.Forms.Label();
            this.SupportLevel = new System.Windows.Forms.TextBox();
            this.Support_Level_Label = new System.Windows.Forms.Label();
            this.CallStartTime = new System.Windows.Forms.DateTimePicker();
            this.Error_label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // File_Name_Label
            // 
            this.File_Name_Label.AutoSize = true;
            this.File_Name_Label.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.File_Name_Label.Location = new System.Drawing.Point(205, 7);
            this.File_Name_Label.Name = "File_Name_Label";
            this.File_Name_Label.Size = new System.Drawing.Size(63, 17);
            this.File_Name_Label.TabIndex = 0;
            this.File_Name_Label.Text = "File name";
            // 
            // File_Name
            // 
            this.File_Name.Location = new System.Drawing.Point(280, 4);
            this.File_Name.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.File_Name.Name = "File_Name";
            this.File_Name.Size = new System.Drawing.Size(233, 25);
            this.File_Name.TabIndex = 1;
            this.File_Name.TextChanged += new System.EventHandler(this.File_Name_TextChanged);
            // 
            // Call_Start_Time_Label
            // 
            this.Call_Start_Time_Label.AutoSize = true;
            this.Call_Start_Time_Label.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Call_Start_Time_Label.Location = new System.Drawing.Point(179, 215);
            this.Call_Start_Time_Label.Name = "Call_Start_Time_Label";
            this.Call_Start_Time_Label.Size = new System.Drawing.Size(88, 17);
            this.Call_Start_Time_Label.TabIndex = 2;
            this.Call_Start_Time_Label.Text = "Call start time";
            // 
            // CallerPhoneNumber
            // 
            this.CallerPhoneNumber.Location = new System.Drawing.Point(280, 72);
            this.CallerPhoneNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CallerPhoneNumber.Name = "CallerPhoneNumber";
            this.CallerPhoneNumber.Size = new System.Drawing.Size(233, 25);
            this.CallerPhoneNumber.TabIndex = 5;
            this.CallerPhoneNumber.TextChanged += new System.EventHandler(this.CallerPhoneNumber_TextChanged);
            // 
            // Caller_Phone_Number_Label
            // 
            this.Caller_Phone_Number_Label.AutoSize = true;
            this.Caller_Phone_Number_Label.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Caller_Phone_Number_Label.Location = new System.Drawing.Point(141, 75);
            this.Caller_Phone_Number_Label.Name = "Caller_Phone_Number_Label";
            this.Caller_Phone_Number_Label.Size = new System.Drawing.Size(131, 17);
            this.Caller_Phone_Number_Label.TabIndex = 4;
            this.Caller_Phone_Number_Label.Text = "Caller phone number";
            // 
            // Category
            // 
            this.Category.Location = new System.Drawing.Point(280, 143);
            this.Category.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Category.Name = "Category";
            this.Category.Size = new System.Drawing.Size(233, 25);
            this.Category.TabIndex = 7;
            this.Category.TextChanged += new System.EventHandler(this.Category_TextChanged);
            // 
            // Category_Label
            // 
            this.Category_Label.AutoSize = true;
            this.Category_Label.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Category_Label.Location = new System.Drawing.Point(209, 147);
            this.Category_Label.Name = "Category_Label";
            this.Category_Label.Size = new System.Drawing.Size(61, 17);
            this.Category_Label.TabIndex = 6;
            this.Category_Label.Text = "Category";
            // 
            // Extension
            // 
            this.Extension.Location = new System.Drawing.Point(280, 38);
            this.Extension.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Extension.Name = "Extension";
            this.Extension.ReadOnly = true;
            this.Extension.Size = new System.Drawing.Size(233, 25);
            this.Extension.TabIndex = 9;
            // 
            // Extension_Label
            // 
            this.Extension_Label.AutoSize = true;
            this.Extension_Label.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Extension_Label.Location = new System.Drawing.Point(204, 41);
            this.Extension_Label.Name = "Extension_Label";
            this.Extension_Label.Size = new System.Drawing.Size(63, 17);
            this.Extension_Label.TabIndex = 8;
            this.Extension_Label.Text = "Extension";
            // 
            // IncomingPhoneNumber
            // 
            this.IncomingPhoneNumber.Location = new System.Drawing.Point(280, 106);
            this.IncomingPhoneNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.IncomingPhoneNumber.Name = "IncomingPhoneNumber";
            this.IncomingPhoneNumber.Size = new System.Drawing.Size(233, 25);
            this.IncomingPhoneNumber.TabIndex = 11;
            this.IncomingPhoneNumber.TextChanged += new System.EventHandler(this.IncomingPhoneNumber_TextChanged);
            // 
            // Incoming_Phone_Number_Label
            // 
            this.Incoming_Phone_Number_Label.AutoSize = true;
            this.Incoming_Phone_Number_Label.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Incoming_Phone_Number_Label.Location = new System.Drawing.Point(121, 109);
            this.Incoming_Phone_Number_Label.Name = "Incoming_Phone_Number_Label";
            this.Incoming_Phone_Number_Label.Size = new System.Drawing.Size(151, 17);
            this.Incoming_Phone_Number_Label.TabIndex = 10;
            this.Incoming_Phone_Number_Label.Text = "Incoming phone number";
            // 
            // SupportLevel
            // 
            this.SupportLevel.Location = new System.Drawing.Point(280, 177);
            this.SupportLevel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SupportLevel.Name = "SupportLevel";
            this.SupportLevel.Size = new System.Drawing.Size(233, 25);
            this.SupportLevel.TabIndex = 13;
            this.SupportLevel.TextChanged += new System.EventHandler(this.SupportLevel_TextChanged);
            // 
            // Support_Level_Label
            // 
            this.Support_Level_Label.AutoSize = true;
            this.Support_Level_Label.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Support_Level_Label.Location = new System.Drawing.Point(181, 181);
            this.Support_Level_Label.Name = "Support_Level_Label";
            this.Support_Level_Label.Size = new System.Drawing.Size(85, 17);
            this.Support_Level_Label.TabIndex = 12;
            this.Support_Level_Label.Text = "Support level";
            // 
            // CallStartTime
            // 
            this.CallStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.CallStartTime.Location = new System.Drawing.Point(280, 215);
            this.CallStartTime.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CallStartTime.MaxDate = new System.DateTime(2030, 12, 31, 0, 0, 0, 0);
            this.CallStartTime.MinDate = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            this.CallStartTime.Name = "CallStartTime";
            this.CallStartTime.Size = new System.Drawing.Size(233, 25);
            this.CallStartTime.TabIndex = 14;
            this.CallStartTime.ValueChanged += new System.EventHandler(this.CallStartTime_ValueChanged);
            // 
            // Error_label
            // 
            this.Error_label.AutoSize = true;
            this.Error_label.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Error_label.ForeColor = System.Drawing.Color.Red;
            this.Error_label.Location = new System.Drawing.Point(45, 250);
            this.Error_label.Name = "Error_label";
            this.Error_label.Size = new System.Drawing.Size(0, 17);
            this.Error_label.TabIndex = 15;
            // 
            // AddMetadataControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(196)))), ((int)(((byte)(210)))));
            this.Controls.Add(this.Error_label);
            this.Controls.Add(this.CallStartTime);
            this.Controls.Add(this.SupportLevel);
            this.Controls.Add(this.Support_Level_Label);
            this.Controls.Add(this.IncomingPhoneNumber);
            this.Controls.Add(this.Incoming_Phone_Number_Label);
            this.Controls.Add(this.Extension);
            this.Controls.Add(this.Extension_Label);
            this.Controls.Add(this.Category);
            this.Controls.Add(this.Category_Label);
            this.Controls.Add(this.CallerPhoneNumber);
            this.Controls.Add(this.Caller_Phone_Number_Label);
            this.Controls.Add(this.Call_Start_Time_Label);
            this.Controls.Add(this.File_Name);
            this.Controls.Add(this.File_Name_Label);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "AddMetadataControl";
            this.Size = new System.Drawing.Size(681, 556);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label File_Name_Label;
        private System.Windows.Forms.TextBox File_Name;
        private System.Windows.Forms.Label Call_Start_Time_Label;
        private System.Windows.Forms.TextBox CallerPhoneNumber;
        private System.Windows.Forms.Label Caller_Phone_Number_Label;
        private System.Windows.Forms.TextBox Category;
        private System.Windows.Forms.Label Category_Label;
        private System.Windows.Forms.TextBox Extension;
        private System.Windows.Forms.Label Extension_Label;
        private System.Windows.Forms.TextBox IncomingPhoneNumber;
        private System.Windows.Forms.Label Incoming_Phone_Number_Label;
        private System.Windows.Forms.TextBox SupportLevel;
        private System.Windows.Forms.Label Support_Level_Label;
        private System.Windows.Forms.DateTimePicker CallStartTime;
        private System.Windows.Forms.Label Error_label;
    }
}
