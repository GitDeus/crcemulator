﻿namespace NumonixCRCEmulatorApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Install_Btn = new System.Windows.Forms.Button();
            this.Title_Label = new System.Windows.Forms.Label();
            this.Validation_Btn = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SidePanel = new System.Windows.Forms.Panel();
            this.Exit = new System.Windows.Forms.Button();
            this.UploadControlLink = new System.Windows.Forms.Button();
            this.InstallationControlLink = new System.Windows.Forms.Button();
            this.ValidationControlLink = new System.Windows.Forms.Button();
            this.header = new System.Windows.Forms.Panel();
            this.Upload_File_Btn = new System.Windows.Forms.Button();
            this.Clear_Session_Btn = new System.Windows.Forms.Button();
            this.step4 = new NumonixCRCEmulatorApp.StepMarkControl();
            this.fileUploadControl1 = new NumonixCRCEmulatorApp.FileUploadControl();
            this.addMetadataControl1 = new NumonixCRCEmulatorApp.AddMetadataControl();
            this.step3 = new NumonixCRCEmulatorApp.StepMarkControl();
            this.step2 = new NumonixCRCEmulatorApp.StepMarkControl();
            this.step1 = new NumonixCRCEmulatorApp.StepMarkControl();
            this.panel1.SuspendLayout();
            this.header.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Honeydew;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(214, 52);
            this.textBox1.Margin = new System.Windows.Forms.Padding(0);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(818, 273);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Paste the installation key here";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Install_Btn
            // 
            this.Install_Btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(79)))), ((int)(((byte)(101)))));
            this.Install_Btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(89)))), ((int)(((byte)(121)))));
            this.Install_Btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Install_Btn.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Install_Btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Install_Btn.Location = new System.Drawing.Point(214, 596);
            this.Install_Btn.Name = "Install_Btn";
            this.Install_Btn.Size = new System.Drawing.Size(149, 36);
            this.Install_Btn.TabIndex = 1;
            this.Install_Btn.Text = "Install";
            this.Install_Btn.UseVisualStyleBackColor = false;
            this.Install_Btn.Click += new System.EventHandler(this.Install_Btn_Click);
            // 
            // Title_Label
            // 
            this.Title_Label.AutoSize = true;
            this.Title_Label.BackColor = System.Drawing.Color.Transparent;
            this.Title_Label.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title_Label.ForeColor = System.Drawing.Color.White;
            this.Title_Label.Location = new System.Drawing.Point(7, 9);
            this.Title_Label.Name = "Title_Label";
            this.Title_Label.Size = new System.Drawing.Size(198, 37);
            this.Title_Label.TabIndex = 2;
            this.Title_Label.Text = "Installation Key";
            this.Title_Label.Click += new System.EventHandler(this.label1_Click);
            // 
            // Validation_Btn
            // 
            this.Validation_Btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(79)))), ((int)(((byte)(101)))));
            this.Validation_Btn.FlatAppearance.BorderSize = 0;
            this.Validation_Btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(89)))), ((int)(((byte)(121)))));
            this.Validation_Btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Validation_Btn.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Validation_Btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Validation_Btn.Location = new System.Drawing.Point(214, 596);
            this.Validation_Btn.Name = "Validation_Btn";
            this.Validation_Btn.Size = new System.Drawing.Size(149, 36);
            this.Validation_Btn.TabIndex = 3;
            this.Validation_Btn.Text = "Validation";
            this.Validation_Btn.UseVisualStyleBackColor = false;
            this.Validation_Btn.Click += new System.EventHandler(this.Validation_Btn_Click);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(214, 328);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox2.Size = new System.Drawing.Size(818, 90);
            this.textBox2.TabIndex = 4;
            this.textBox2.Text = "Logs";
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(24)))), ((int)(((byte)(28)))));
            this.panel1.Controls.Add(this.SidePanel);
            this.panel1.Controls.Add(this.Exit);
            this.panel1.Controls.Add(this.UploadControlLink);
            this.panel1.Controls.Add(this.InstallationControlLink);
            this.panel1.Controls.Add(this.ValidationControlLink);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 642);
            this.panel1.TabIndex = 5;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // SidePanel
            // 
            this.SidePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(79)))), ((int)(((byte)(101)))));
            this.SidePanel.Location = new System.Drawing.Point(0, 71);
            this.SidePanel.Name = "SidePanel";
            this.SidePanel.Size = new System.Drawing.Size(10, 62);
            this.SidePanel.TabIndex = 7;
            // 
            // Exit
            // 
            this.Exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(24)))), ((int)(((byte)(28)))));
            this.Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exit.FlatAppearance.BorderSize = 0;
            this.Exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(196)))), ((int)(((byte)(210)))));
            this.Exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(79)))), ((int)(((byte)(101)))));
            this.Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exit.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Exit.Location = new System.Drawing.Point(9, 580);
            this.Exit.Margin = new System.Windows.Forms.Padding(0);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(191, 62);
            this.Exit.TabIndex = 3;
            this.Exit.Text = "   Exit";
            this.Exit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Exit.UseVisualStyleBackColor = false;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // UploadControlLink
            // 
            this.UploadControlLink.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(24)))), ((int)(((byte)(28)))));
            this.UploadControlLink.Cursor = System.Windows.Forms.Cursors.Hand;
            this.UploadControlLink.FlatAppearance.BorderSize = 0;
            this.UploadControlLink.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(196)))), ((int)(((byte)(210)))));
            this.UploadControlLink.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(79)))), ((int)(((byte)(101)))));
            this.UploadControlLink.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UploadControlLink.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UploadControlLink.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.UploadControlLink.Location = new System.Drawing.Point(9, 196);
            this.UploadControlLink.Margin = new System.Windows.Forms.Padding(0);
            this.UploadControlLink.Name = "UploadControlLink";
            this.UploadControlLink.Size = new System.Drawing.Size(191, 62);
            this.UploadControlLink.TabIndex = 2;
            this.UploadControlLink.Text = "    Upload .wav file";
            this.UploadControlLink.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.UploadControlLink.UseVisualStyleBackColor = false;
            this.UploadControlLink.Click += new System.EventHandler(this.UploadControlLink_Click);
            // 
            // InstallationControlLink
            // 
            this.InstallationControlLink.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(24)))), ((int)(((byte)(28)))));
            this.InstallationControlLink.FlatAppearance.BorderSize = 0;
            this.InstallationControlLink.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(196)))), ((int)(((byte)(210)))));
            this.InstallationControlLink.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(79)))), ((int)(((byte)(101)))));
            this.InstallationControlLink.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InstallationControlLink.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstallationControlLink.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.InstallationControlLink.Location = new System.Drawing.Point(9, 134);
            this.InstallationControlLink.Margin = new System.Windows.Forms.Padding(0);
            this.InstallationControlLink.Name = "InstallationControlLink";
            this.InstallationControlLink.Size = new System.Drawing.Size(191, 62);
            this.InstallationControlLink.TabIndex = 1;
            this.InstallationControlLink.Text = "    Installation";
            this.InstallationControlLink.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InstallationControlLink.UseVisualStyleBackColor = false;
            this.InstallationControlLink.Click += new System.EventHandler(this.InstallationControlLink_Click);
            // 
            // ValidationControlLink
            // 
            this.ValidationControlLink.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(24)))), ((int)(((byte)(28)))));
            this.ValidationControlLink.FlatAppearance.BorderSize = 0;
            this.ValidationControlLink.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(196)))), ((int)(((byte)(210)))));
            this.ValidationControlLink.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(79)))), ((int)(((byte)(101)))));
            this.ValidationControlLink.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ValidationControlLink.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValidationControlLink.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ValidationControlLink.Location = new System.Drawing.Point(9, 72);
            this.ValidationControlLink.Margin = new System.Windows.Forms.Padding(0);
            this.ValidationControlLink.Name = "ValidationControlLink";
            this.ValidationControlLink.Size = new System.Drawing.Size(191, 62);
            this.ValidationControlLink.TabIndex = 0;
            this.ValidationControlLink.Text = "    Validation";
            this.ValidationControlLink.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ValidationControlLink.UseVisualStyleBackColor = false;
            this.ValidationControlLink.Click += new System.EventHandler(this.ValidationControlLink_Click);
            // 
            // header
            // 
            this.header.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(79)))), ((int)(((byte)(101)))));
            this.header.Controls.Add(this.Title_Label);
            this.header.Location = new System.Drawing.Point(200, 0);
            this.header.Margin = new System.Windows.Forms.Padding(0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(832, 46);
            this.header.TabIndex = 6;
            this.header.MouseDown += new System.Windows.Forms.MouseEventHandler(this.header_MouseDown);
            this.header.MouseMove += new System.Windows.Forms.MouseEventHandler(this.header_MouseMove);
            // 
            // Upload_File_Btn
            // 
            this.Upload_File_Btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(79)))), ((int)(((byte)(101)))));
            this.Upload_File_Btn.FlatAppearance.BorderSize = 0;
            this.Upload_File_Btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(89)))), ((int)(((byte)(121)))));
            this.Upload_File_Btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Upload_File_Btn.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Upload_File_Btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Upload_File_Btn.Location = new System.Drawing.Point(214, 596);
            this.Upload_File_Btn.Name = "Upload_File_Btn";
            this.Upload_File_Btn.Size = new System.Drawing.Size(149, 36);
            this.Upload_File_Btn.TabIndex = 13;
            this.Upload_File_Btn.Text = "Upload";
            this.Upload_File_Btn.UseVisualStyleBackColor = false;
            this.Upload_File_Btn.Click += new System.EventHandler(this.Upload_File_Btn_Click);
            // 
            // Clear_Session_Btn
            // 
            this.Clear_Session_Btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(79)))), ((int)(((byte)(101)))));
            this.Clear_Session_Btn.FlatAppearance.BorderSize = 0;
            this.Clear_Session_Btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(89)))), ((int)(((byte)(121)))));
            this.Clear_Session_Btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Clear_Session_Btn.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Clear_Session_Btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Clear_Session_Btn.Location = new System.Drawing.Point(385, 596);
            this.Clear_Session_Btn.Name = "Clear_Session_Btn";
            this.Clear_Session_Btn.Size = new System.Drawing.Size(149, 36);
            this.Clear_Session_Btn.TabIndex = 15;
            this.Clear_Session_Btn.Text = "Clear session";
            this.Clear_Session_Btn.UseVisualStyleBackColor = false;
            this.Clear_Session_Btn.Click += new System.EventHandler(this.Clear_Session_Btn_Click);
            // 
            // step4
            // 
            this.step4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.step4.Location = new System.Drawing.Point(215, 552);
            this.step4.Name = "step4";
            this.step4.Size = new System.Drawing.Size(804, 37);
            this.step4.TabIndex = 14;
            // 
            // fileUploadControl1
            // 
            this.fileUploadControl1.BackColor = System.Drawing.Color.Transparent;
            this.fileUploadControl1.Location = new System.Drawing.Point(769, 150);
            this.fileUploadControl1.Name = "fileUploadControl1";
            this.fileUploadControl1.Size = new System.Drawing.Size(221, 33);
            this.fileUploadControl1.TabIndex = 12;
            // 
            // addMetadataControl1
            // 
            this.addMetadataControl1.BackColor = System.Drawing.Color.Transparent;
            this.addMetadataControl1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.addMetadataControl1.Location = new System.Drawing.Point(214, 52);
            this.addMetadataControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.addMetadataControl1.Name = "addMetadataControl1";
            this.addMetadataControl1.Size = new System.Drawing.Size(818, 277);
            this.addMetadataControl1.TabIndex = 11;
            // 
            // step3
            // 
            this.step3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.step3.Location = new System.Drawing.Point(215, 509);
            this.step3.Name = "step3";
            this.step3.Size = new System.Drawing.Size(804, 37);
            this.step3.TabIndex = 9;
            this.step3.Load += new System.EventHandler(this.stepMarkControl1_Load);
            // 
            // step2
            // 
            this.step2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.step2.Location = new System.Drawing.Point(215, 466);
            this.step2.Name = "step2";
            this.step2.Size = new System.Drawing.Size(804, 37);
            this.step2.TabIndex = 8;
            this.step2.Load += new System.EventHandler(this.step2_Load);
            // 
            // step1
            // 
            this.step1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.step1.Location = new System.Drawing.Point(215, 423);
            this.step1.Name = "step1";
            this.step1.Size = new System.Drawing.Size(804, 37);
            this.step1.TabIndex = 7;
            this.step1.Load += new System.EventHandler(this.step1_Load);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(238)))), ((int)(((byte)(244)))));
            this.ClientSize = new System.Drawing.Size(1031, 642);
            this.Controls.Add(this.Clear_Session_Btn);
            this.Controls.Add(this.step4);
            this.Controls.Add(this.Upload_File_Btn);
            this.Controls.Add(this.fileUploadControl1);
            this.Controls.Add(this.addMetadataControl1);
            this.Controls.Add(this.step3);
            this.Controls.Add(this.step2);
            this.Controls.Add(this.step1);
            this.Controls.Add(this.header);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.Validation_Btn);
            this.Controls.Add(this.Install_Btn);
            this.Controls.Add(this.textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.header.ResumeLayout(false);
            this.header.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button Install_Btn;
        private System.Windows.Forms.Label Title_Label;
        private System.Windows.Forms.Button Validation_Btn;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button ValidationControlLink;
        private System.Windows.Forms.Button UploadControlLink;
        private System.Windows.Forms.Button InstallationControlLink;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Panel header;
        private System.Windows.Forms.Panel SidePanel;
        private StepMarkControl step1;
        private StepMarkControl step2;
        private StepMarkControl step3;
        private AddMetadataControl addMetadataControl1;
        private FileUploadControl fileUploadControl1;
        private System.Windows.Forms.Button Upload_File_Btn;
        private StepMarkControl step4;
        private System.Windows.Forms.Button Clear_Session_Btn;
    }
}

