﻿using Newtonsoft.Json;
using NumonixCRCEmulator;
using NumonixCRCEmulator.Encryption;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace NumonixCRCEmulatorApp
{
    public partial class Form1 : Form
    {
        private readonly StringBuilder _logs = new StringBuilder();

        private Point _lastPoint;

        private DemoWizard _wizard;

        private byte[] FileBytes { get; set; }

        private string _installationKeyBase64String;

        private string _clientPrivateKey;

        private string _sessionPublicKey;

        private string _authToken;


        public Form1()
        {
            InitializeComponent();
            SidePanel.Height = ValidationControlLink.Height;
        }
        public void LoadSession()
        {
            Title_Label.Text = "Validation of the installation key";
            Upload_File_Btn.Visible = false;
            Upload_File_Btn.Enabled = false;
            Install_Btn.Enabled = false;
            Install_Btn.Visible = false;
            addMetadataControl1.Visible = false;
            fileUploadControl1.Visible = false;
            _authToken = null;
            _sessionPublicKey = null;
            _clientPrivateKey = null;
            //_clientPrivateKey = File.Exists(Environment.CurrentDirectory + "/clientPrivateKey") ? File.ReadAllText(Environment.CurrentDirectory + "/clientPrivateKey") : null;
            //_sessionPublicKey = File.Exists(Environment.CurrentDirectory + "/sessionPK") ? File.ReadAllText(Environment.CurrentDirectory + "/sessionPK") : null;
            //_authToken = File.Exists(Environment.CurrentDirectory + "/token") ? File.ReadAllText(Environment.CurrentDirectory + "/token") : null;
            //_installationKeyBase64String = File.Exists(Environment.CurrentDirectory + "/installationKey") ? File.ReadAllText(Environment.CurrentDirectory + "/installationKey") : null;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadSession();

            fileUploadControl1.FileLoad += (o, args) =>
            {
                var mockFileInfo = new CallRecordingFileInfo
                {
                    Name = Guid.NewGuid().ToString(),
                    CallStartTime = DateTime.UtcNow,
                    CallerPhoneNumber = "+1 212-732-5692",
                    Category = "Technical Issue",
                    Extension = ".wav",
                    IncomingPhoneNumber = "+1 212-279-1289",
                    SupportLevel = "L2 Support"
                };

                var fileParams = args.FileName.Split('.');
                if (fileParams.Length == 1)
                {
                    mockFileInfo.Name = fileParams[0];
                    mockFileInfo.Extension = ".wav";
                }
                else {

                    mockFileInfo.Name = fileParams[0];
                    mockFileInfo.Extension = '.' + fileParams[1];
                }

 
                addMetadataControl1.Set(mockFileInfo);
                Upload_File_Btn.Enabled = true;
                FileBytes = args.FileBytes;
            };

            if (!string.IsNullOrEmpty(_installationKeyBase64String))
            {
                textBox1.Text = _installationKeyBase64String;
            }
            _wizard = new DemoWizard
            {
                Steps = new Step[]
                {
                    new Step() {Title = "Validation of the installation key"},
                    new Step() {Title = "Using of the installation key"},
                    new Step() {Title = "Uploading of the metadata"},
                    new Step() {Title = "Uploading of the .wav file"},
                }
            };



            if (!string.IsNullOrEmpty(_authToken))
            {
                WriteLog("Installation key is was used \t\r\nAuth Token " + _authToken);
                _wizard.Steps[1].Status = State.Success;
            }

            step1.StatusChanges(this, _wizard.Steps[0]);
            step2.StatusChanges(this, _wizard.Steps[1]);
            step3.StatusChanges(this, _wizard.Steps[2]);
            step4.StatusChanges(this, _wizard.Steps[3]);

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            _installationKeyBase64String = textBox1.Text;
        }

        /// <summary>
        /// Use installation key.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button1_Click(object sender, EventArgs e)
        {

        }


        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void UploadControlLink_Click(object sender, EventArgs e)
        {
            SidePanel.Height = UploadControlLink.Height;
            SidePanel.Top = UploadControlLink.Top;
            addMetadataControl1.Visible = true;
            fileUploadControl1.Visible = true;
            Install_Btn.Visible = false;
            Validation_Btn.Visible = false;
            Upload_File_Btn.Visible = true;
            Title_Label.Text = "Upload of the .wav file";
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            SidePanel.Height = Exit.Height;
            SidePanel.Top = Exit.Top;
            Application.Exit();
        }

        private void ValidationControlLink_Click(object sender, EventArgs e)
        {
            SidePanel.Height = ValidationControlLink.Height;
            SidePanel.Top = ValidationControlLink.Top;
            addMetadataControl1.Visible = false;
            fileUploadControl1.Visible = false;
            Install_Btn.Visible = false;
            Validation_Btn.Visible = true;
            Upload_File_Btn.Visible = false;
            Title_Label.Text = "Validation of the installation key";
        }

        private void InstallationControlLink_Click(object sender, EventArgs e)
        {
            SidePanel.Height = InstallationControlLink.Height;
            SidePanel.Top = InstallationControlLink.Top;
            addMetadataControl1.Visible = false;
            fileUploadControl1.Visible = false;
            Validation_Btn.Visible = false;
            Install_Btn.Visible = true;
            Upload_File_Btn.Visible = false;
            Title_Label.Text = "Using of the installation key";
        }


        /// <summary>
        /// Validation installation key.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>

        private async void Validation_Btn_Click(object sender, EventArgs e)
        {
            try
            {
                var json = Encoding.UTF8.GetString(Convert.FromBase64String(_installationKeyBase64String));
                var installationKey = JsonConvert.DeserializeObject<InstallationKey>(json);
                var validationResult = await NumonixCRCEmulator.Program.Validation(installationKey.TenantId, installationKey.Endpoint, _installationKeyBase64String);

             
                _wizard.Steps[0].Status = validationResult.Success ? State.Success : State.Error;
                _wizard.Steps[0].Errors = validationResult.StatusCodes.ToDictionary(statusCode => statusCode, GetDescriptionByStatusCode);


                if (validationResult.Success)
                {
                    _wizard.Steps[0].Title = "Installation key is valid";
                    Install_Btn.Enabled = true;
                    WriteLog(_wizard.Steps[0].Title);
                }
                else
                {
                    Install_Btn.Enabled = false;
                    _wizard.Steps[0].Title = "Invalid of the installation key" + string.Join("\t", _wizard.Steps[0].Errors.Select(kv => string.Format("\t{0}", kv.Value)));
                    WriteLog("Invalid of the installation key" + string.Join("\t", _wizard.Steps[0].Errors.Select(kv => string.Format("\r\n{0}", kv.Value))));
                }

                
            }
            catch (Exception ex)
            {
                Install_Btn.Enabled = false;
                _wizard.Steps[0].Status = State.Error;
                _wizard.Steps[0].Title = "Invalid of the installation key";
                WriteLog(_wizard.Steps[0].Title);
            }
   
            step1.StatusChanges(this, _wizard.Steps[0]);
        }

        private async void Install_Btn_Click(object sender, EventArgs e)
        {
            try
            {
                WriteLog(_wizard.Steps[1].Title);
                if (!string.IsNullOrEmpty(_authToken))
                {
                    _wizard.Steps[1].Status = State.Success;
                    WriteLog("Installation key is was used \t\r\nAuth Token " + _authToken);
                    step2.StatusChanges(this, _wizard.Steps[1]);
                    return;
                }

                if (!NumonixCRCEmulator.Program.InstallationKeyIsValid(installationKeyBase64: _installationKeyBase64String))
                {
                    _wizard.Steps[1].Status = State.Error;
                    WriteLog("Invalid of the installation key!");
                }
                else
                {
                    try
                    {
                        var json = Encoding.UTF8.GetString(Convert.FromBase64String(_installationKeyBase64String));
                        var installationKey = JsonConvert.DeserializeObject<InstallationKeyOut>(json);


                        using (var clientRsa = new RSACryptoServiceProvider(2048))
                        {

                            var privateClientKey = RSAKeys.ExportPrivateKey(clientRsa);
                            var publicClientKey = RSAKeys.ExportPublicKey(clientRsa);


                            var serverRsa = RSAKeys.ImportPublicKey(installationKey.PublicKey);
                            var secret = Security.GenerateRandomString();
                            var encryptedClientKeyInfo = new ClientKeyInfo(installationKey.Id,
                                NumonixCRCEmulator.Encryption.RSA.Encrypt(secret, serverRsa),
                                AES.Encrypt(publicClientKey, secret));


                            var exchangeResult = await NumonixCRCEmulator.Program.Exchange(installationKey.TenantId, installationKey.Endpoint, privateClientKey, encryptedClientKeyInfo);


                            _wizard.Steps[1].Status = exchangeResult.Success ? State.Success : State.Error;
                            _wizard.Steps[1].Errors = exchangeResult.StatusCodes.ToDictionary(statusCode=> statusCode, GetDescriptionByStatusCode);
                            _wizard.Steps[1].Title = $"Installation key successfully used, authorization token received";
                            WriteLog($"Installation key successfully used, authorization token received" + "\r\nAuth token \t" + exchangeResult.Data.Token);
                            _sessionPublicKey = exchangeResult.Data.Key;
                            _authToken = exchangeResult.Data.Token;
                            _clientPrivateKey = privateClientKey;

                            File.WriteAllText(Environment.CurrentDirectory + "/sessionPK", exchangeResult.Data.Key);
                            File.WriteAllText(Environment.CurrentDirectory + "/token", exchangeResult.Data.Token);
                            File.WriteAllText(Environment.CurrentDirectory + "/installationKey", _installationKeyBase64String);
                            File.WriteAllText(Environment.CurrentDirectory + "/clientPrivateKey", privateClientKey);
                        }

                    }
                    catch (Exception ex)
                    {
                        _wizard.Steps[1].Status = State.Error;
                        _wizard.Steps[1].Title = "Invalid of the installation key!\t" + ex.Message;
                        WriteLog(_wizard.Steps[1].Title);
                    }
    
                }
                step2.StatusChanges(this, _wizard.Steps[1]);

            }
            catch (Exception ex)
            {
                _wizard.Steps[1].Status = State.Error;
                _wizard.Steps[1].Title = $"Invalid of the installation key!";
                WriteLog(_wizard.Steps[1].Title);
            }
  
            step2.StatusChanges(this, _wizard.Steps[1]);
        }

        private void step1_Load(object sender, EventArgs e)
        {

        }

        private void step2_Load(object sender, EventArgs e)
        {

        }

        private void stepMarkControl1_Load(object sender, EventArgs e)
        {

        }

        private void fileUploadControl1_Load(object sender, EventArgs e)
        {

        }



        private string GetDescriptionByStatusCode(InstallationKeyValidationStatus statusCode)
        {
            ValidationDescriptions.TryGetValue((int)statusCode, out var description);
            return description;
        }

        private static readonly Dictionary<int, string> ValidationDescriptions = new Dictionary<int, string>()
        {
            { 0, "Success" },
            { 1, "License limit reached" },
            { 2, "Installation key expired"},
            { 3, "Installation key not found"},
            { 4, "Installation key deactivated"}
        };

        public void MetadataValidation()
        {
            if (string.IsNullOrEmpty(addMetadataControl1.Metadata.CallerPhoneNumber)) throw new ArgumentException("Caller phone number required");
            if (addMetadataControl1.Metadata.CallStartTime > DateTime.UtcNow) throw new ArgumentException("Call start time not yet come");
            if (string.IsNullOrEmpty(addMetadataControl1.Metadata.Category)) throw new ArgumentException("Category required");
            if (string.IsNullOrEmpty(addMetadataControl1.Metadata.Extension)) throw new ArgumentException("Extension required");
            if (string.IsNullOrEmpty(addMetadataControl1.Metadata.IncomingPhoneNumber)) throw new ArgumentException("Incoming phone number required");
            if (string.IsNullOrEmpty(addMetadataControl1.Metadata.SupportLevel)) throw new ArgumentException("Support level required");
        }




        private void Upload_File_Btn_Click(object sender, EventArgs e)
        {
            try
            {
                MetadataValidation();
            }
            catch (ArgumentException error)
            {
                addMetadataControl1.ValidationError(error.Message);
                return;
            }

            try
            {


                var installationKey = JsonConvert.DeserializeObject<InstallationKeyOut>(Encoding.UTF8.GetString(Convert.FromBase64String(_installationKeyBase64String)));

                var baseEndpoint = installationKey.Endpoint;

                var privateClientKey = RSAKeys.ImportPrivateKey(_clientPrivateKey);

                var serverRsa = RSAKeys.ImportPublicKey(_sessionPublicKey);




                var secret = Security.GenerateRandomString();
                var encryptedMetadata =
                    AES.Encrypt(
                        JsonConvert.SerializeObject(addMetadataControl1.Metadata), secret);

                var encryptedAesSecret = NumonixCRCEmulator.Encryption.RSA.Encrypt(secret, serverRsa);


                Title_Label.Text = _wizard.Steps[2].Title = "Uploading of the metadata...";
                WriteLog(_wizard.Steps[2].Title);
                step3.StatusChanges(this, _wizard.Steps[2]);
                var metadataResponse = NumonixCRCEmulator.Program.AddMetadata(baseEndpoint, encryptedMetadata, encryptedAesSecret, _authToken);

                if (metadataResponse.StatusCode == HttpStatusCode.Unauthorized)
                {
     
                    _wizard.Steps[2].Status = State.Success;
                    _wizard.Steps[2].Errors = new[] { InstallationKeyValidationStatus.Expired }.ToDictionary(statusCode => statusCode, v => "Session has expired");
                    Title_Label.Text = _wizard.Steps[2].Title = "Session has expired";
                    WriteLog(_wizard.Steps[2].Title);
                    step3.StatusChanges(this, _wizard.Steps[2]);
                }
                else if (metadataResponse.IsSuccessStatusCode)
                {
                    var encryptedResponse = metadataResponse.Content.ReadAsAsync<EncryptedString>()
                        .GetAwaiter()
                        .GetResult();

                    var serverSecret = NumonixCRCEmulator.Encryption.RSA.Decrypt(encryptedResponse.Key, privateClientKey);

                    var metadataId = JsonConvert.DeserializeObject<Guid>(AES.Decrypt(encryptedResponse.Encrypted, serverSecret));


                    _wizard.Steps[2].Status = State.Success;
                    _wizard.Steps[2].Errors = new[] { InstallationKeyValidationStatus.Success }.ToDictionary(statusCode => statusCode, v => "Metadata success added");
                    Title_Label.Text = _wizard.Steps[2].Title = "Uploading of the metadata";
                    WriteLog(_wizard.Steps[2].Title);
                    step3.StatusChanges(this, _wizard.Steps[2]);
                    #region Upload call recording wav file 


                    var callRecordingEncryptedBytes = AES.Encrypt(FileBytes, secret);
                    var fileName = addMetadataControl1.Metadata.Name;
                    var uploadResponse = NumonixCRCEmulator.Program.UploadCallRecording(baseEndpoint, encryptedAesSecret, metadataId, _authToken, fileName, callRecordingEncryptedBytes);

                    if (uploadResponse.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        _wizard.Steps[3].Status = State.Error;
                        _wizard.Steps[3].Errors = new[] { InstallationKeyValidationStatus.Expired }.ToDictionary(statusCode => statusCode, v => "Session has expired");
                        Title_Label.Text = _wizard.Steps[3].Title = $"Session has expired";
                    }
                    else if (uploadResponse.IsSuccessStatusCode)
                    {
                        _wizard.Steps[3].Status = State.Success;
                        _wizard.Steps[3].Errors = new[] { InstallationKeyValidationStatus.Success }.ToDictionary(statusCode => statusCode, v => "Success upload");
                        Title_Label.Text = _wizard.Steps[3].Title = $"Success uploaded";
                    }

                    else {
                        _wizard.Steps[3].Status = State.Error;
                        _wizard.Steps[3].Errors = new[] { InstallationKeyValidationStatus.Invalid }.ToDictionary(statusCode => statusCode, v => uploadResponse.StatusCode + "\t\r\n" + uploadResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult());
                        Title_Label.Text = _wizard.Steps[3].Title = $"Error uploading file";
                    }
                    WriteLog(_wizard.Steps[3].Title);
                    #endregion

                }
                else
                { 
                    _wizard.Steps[3].Status = State.Error;
                    _wizard.Steps[3].Errors = new[] { InstallationKeyValidationStatus.Invalid }.ToDictionary(statusCode => statusCode, v => metadataResponse.ReasonPhrase);
                    Title_Label.Text = _wizard.Steps[3].Title = "Error";
                    WriteLog(_wizard.Steps[3].Title);
                }
  

            }
            catch (Exception ex)
            {
                _wizard.Steps[3].Status = State.Error;
                _wizard.Steps[3].Errors = new[] { InstallationKeyValidationStatus.Invalid }.ToDictionary(statusCode => statusCode, v => ex.Message);
                Title_Label.Text = _wizard.Steps[3].Title = "Installation key not used";
                WriteLog("Installation key not used");
            }
            step4.StatusChanges(this, _wizard.Steps[3]);
        }

        private void Clear_Session_Btn_Click(object sender, EventArgs e)
        {
            ValidationControlLink_Click(sender, e);
            File.Delete(Environment.CurrentDirectory + "/sessionPK");
            File.Delete(Environment.CurrentDirectory + "/token");
            File.Delete(Environment.CurrentDirectory + "/installationKey");
            File.Delete(Environment.CurrentDirectory + "/clientPrivateKey");
            LoadSession();
            _logs.Clear();
            textBox2.Text = "";
            _wizard = new DemoWizard
            {
                Steps = new Step[]
    {
                    new Step() {Title = "Validation of the installation key"},
                    new Step() {Title = "Using of the installation key"},
                    new Step() {Title = "Uploading of the metadata"},
                    new Step() {Title = "Uploading of the .wav file"},
    }
            };

            step1.StatusChanges(this, _wizard.Steps[0]);
            step2.StatusChanges(this, _wizard.Steps[1]);
            step3.StatusChanges(this, _wizard.Steps[2]);
            step4.StatusChanges(this, _wizard.Steps[3]);
        }

        public void WriteLog(string message)
        {
            _logs.Append("\r\n");
            _logs.Append(DateTime.UtcNow.ToString());
            _logs.Append("  ");
            _logs.Append(message);
  
            textBox2.Text = _logs.ToString();
        }

        #region Custom behavior of moving windows
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - _lastPoint.X;
                this.Top += e.Y - _lastPoint.Y;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            _lastPoint = new Point(e.X, e.Y);
        }

        private void header_MouseDown(object sender, MouseEventArgs e)
        {
            _lastPoint = new Point(e.X, e.Y);
        }

        private void header_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - _lastPoint.X;
                this.Top += e.Y - _lastPoint.Y;
            }
        }
        #endregion
    }
}
