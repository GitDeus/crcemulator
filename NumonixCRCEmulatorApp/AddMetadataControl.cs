﻿using NumonixCRCEmulator;
using System;
using System.Windows.Forms;

namespace NumonixCRCEmulatorApp
{
    public partial class AddMetadataControl : UserControl
    {
        public CallRecordingFileInfo Metadata { get; private set; }
        public AddMetadataControl()
        {
            InitializeComponent();
            CallStartTime.Format = DateTimePickerFormat.Custom;
            CallStartTime.CustomFormat = "MM.dd.yyyy hh:mm:ss";
            Metadata = new CallRecordingFileInfo
            {
                CallerPhoneNumber = CallerPhoneNumber.Text.TrimStart().TrimEnd(),
                CallStartTime = CallStartTime.Value,
                Category = Category.Text.TrimStart().TrimEnd(),
                Extension = Extension.Text.TrimStart().TrimEnd(),
                IncomingPhoneNumber = IncomingPhoneNumber.Text.TrimStart().TrimEnd(),
                Name = File_Name.Text.TrimStart().TrimEnd(),
                SupportLevel = SupportLevel.Text.TrimStart().TrimEnd()
            };
        }
        public void Set(CallRecordingFileInfo metadata)
        {
            Metadata = metadata;
            File_Name.Text = Metadata.Name;
            Extension.Text = Metadata.Extension;
            CallerPhoneNumber.Text = Metadata.CallerPhoneNumber;
            CallStartTime.Value = Metadata.CallStartTime;
            Category.Text = Metadata.Category;
            Extension.Text = Metadata.Extension;
            IncomingPhoneNumber.Text = Metadata.IncomingPhoneNumber;
            File_Name.Text = Metadata.Name;
            SupportLevel.Text = Metadata.SupportLevel;
        }

        public void ValidationError(string errorMessage)
        {
            Error_label.Text = errorMessage;
        }

        private void File_Name_TextChanged(object sender, EventArgs e)
        {
            Metadata.Name = File_Name.Text.TrimStart().TrimEnd();
        }

        private void CallerPhoneNumber_TextChanged(object sender, EventArgs e)
        {
            Metadata.CallerPhoneNumber = CallerPhoneNumber.Text.TrimStart().TrimEnd();
        }

        private void IncomingPhoneNumber_TextChanged(object sender, EventArgs e)
        {
            Metadata.IncomingPhoneNumber = IncomingPhoneNumber.Text.TrimStart().TrimEnd();
        }

        private void Category_TextChanged(object sender, EventArgs e)
        {
            Metadata.Category = Category.Text.TrimStart().TrimEnd();
        }

        private void SupportLevel_TextChanged(object sender, EventArgs e)
        {
            Metadata.SupportLevel = SupportLevel.Text.TrimStart().TrimEnd();
        }

        private void CallStartTime_ValueChanged(object sender, EventArgs e)
        {
            Metadata.CallStartTime = CallStartTime.Value;
        }
    }
}
