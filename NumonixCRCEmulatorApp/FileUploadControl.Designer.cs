﻿namespace NumonixCRCEmulatorApp
{
    partial class FileUploadControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Select_File_Btn = new System.Windows.Forms.Button();
            this.File_Label = new System.Windows.Forms.Label();
            this.File_Text = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Select_File_Btn
            // 
            this.Select_File_Btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(79)))), ((int)(((byte)(101)))));
            this.Select_File_Btn.FlatAppearance.BorderSize = 0;
            this.Select_File_Btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(89)))), ((int)(((byte)(121)))));
            this.Select_File_Btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Select_File_Btn.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Select_File_Btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Select_File_Btn.Location = new System.Drawing.Point(20, 0);
            this.Select_File_Btn.Margin = new System.Windows.Forms.Padding(0);
            this.Select_File_Btn.Name = "Select_File_Btn";
            this.Select_File_Btn.Size = new System.Drawing.Size(182, 34);
            this.Select_File_Btn.TabIndex = 0;
            this.Select_File_Btn.Text = "Select file";
            this.Select_File_Btn.UseVisualStyleBackColor = false;
            this.Select_File_Btn.Click += new System.EventHandler(this.Select_File_Btn_Click);
            // 
            // File_Label
            // 
            this.File_Label.AutoSize = true;
            this.File_Label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.File_Label.Location = new System.Drawing.Point(16, 40);
            this.File_Label.Name = "File_Label";
            this.File_Label.Size = new System.Drawing.Size(80, 21);
            this.File_Label.TabIndex = 1;
            this.File_Label.Text = "File name:";
            // 
            // File_Text
            // 
            this.File_Text.Location = new System.Drawing.Point(102, 43);
            this.File_Text.Name = "File_Text";
            this.File_Text.ReadOnly = true;
            this.File_Text.Size = new System.Drawing.Size(100, 20);
            this.File_Text.TabIndex = 2;
            // 
            // FileUploadControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.File_Text);
            this.Controls.Add(this.File_Label);
            this.Controls.Add(this.Select_File_Btn);
            this.Name = "FileUploadControl";
            this.Size = new System.Drawing.Size(221, 64);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Select_File_Btn;
        private System.Windows.Forms.Label File_Label;
        private System.Windows.Forms.TextBox File_Text;
    }
}
