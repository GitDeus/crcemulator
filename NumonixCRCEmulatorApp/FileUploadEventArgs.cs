﻿using System;

namespace NumonixCRCEmulatorApp
{
    public class FileUploadEventArgs : EventArgs
    {
        public FileUploadEventArgs(string fileName, byte[] bytes)
        {
            FileName = fileName;

            FileBytes = bytes;
        }
        public string FileName { get; }

        public byte[] FileBytes { get; }
    }
}
