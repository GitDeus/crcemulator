﻿namespace NumonixCRCEmulatorApp
{
    partial class StepMarkControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StepMarkControl));
            this.Success_Status_Image = new System.Windows.Forms.PictureBox();
            this.Status_Description = new System.Windows.Forms.TextBox();
            this.Error_Status_Img = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Success_Status_Image)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Error_Status_Img)).BeginInit();
            this.SuspendLayout();
            // 
            // Success_Status_Image
            // 
            this.Success_Status_Image.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Success_Status_Image.BackColor = System.Drawing.Color.Transparent;
            this.Success_Status_Image.Image = ((System.Drawing.Image)(resources.GetObject("Success_Status_Image.Image")));
            this.Success_Status_Image.Location = new System.Drawing.Point(0, 0);
            this.Success_Status_Image.Name = "Success_Status_Image";
            this.Success_Status_Image.Size = new System.Drawing.Size(134, 129);
            this.Success_Status_Image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Success_Status_Image.TabIndex = 1;
            this.Success_Status_Image.TabStop = false;
            this.Success_Status_Image.Visible = false;
            // 
            // Status_Description
            // 
            this.Status_Description.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Status_Description.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Status_Description.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Status_Description.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Status_Description.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Status_Description.Location = new System.Drawing.Point(143, 6);
            this.Status_Description.Multiline = true;
            this.Status_Description.Name = "Status_Description";
            this.Status_Description.ReadOnly = true;
            this.Status_Description.ShortcutsEnabled = false;
            this.Status_Description.Size = new System.Drawing.Size(849, 123);
            this.Status_Description.TabIndex = 2;
            this.Status_Description.Text = "Text";
            this.Status_Description.TextChanged += new System.EventHandler(this.Status_Description_TextChanged);
            // 
            // Error_Status_Img
            // 
            this.Error_Status_Img.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Error_Status_Img.BackColor = System.Drawing.Color.Transparent;
            this.Error_Status_Img.Image = ((System.Drawing.Image)(resources.GetObject("Error_Status_Img.Image")));
            this.Error_Status_Img.Location = new System.Drawing.Point(0, 1);
            this.Error_Status_Img.Margin = new System.Windows.Forms.Padding(0);
            this.Error_Status_Img.Name = "Error_Status_Img";
            this.Error_Status_Img.Size = new System.Drawing.Size(128, 128);
            this.Error_Status_Img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Error_Status_Img.TabIndex = 3;
            this.Error_Status_Img.TabStop = false;
            this.Error_Status_Img.Visible = false;
            // 
            // StepMarkControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.Success_Status_Image);
            this.Controls.Add(this.Status_Description);
            this.Controls.Add(this.Error_Status_Img);
            this.Name = "StepMarkControl";
            this.Size = new System.Drawing.Size(992, 129);
            ((System.ComponentModel.ISupportInitialize)(this.Success_Status_Image)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Error_Status_Img)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox Success_Status_Image;
        public System.Windows.Forms.TextBox Status_Description;
        private System.Windows.Forms.PictureBox Error_Status_Img;
    }
}
